function fish_user_key_bindings
	bind \cs ".runsudo"
end

function fish_greeting
  set hostname (hostname)
  set -l red (set_color -o red)
  set -l usercolor (set_color -o blue)
  set -l blue (set_color -o blue)
	echo $usercolor
	echo "Welcome to $hostname"
	echo
	echo "Word for today: $blue"
	fortune
	echo
end

# OPAM configuration
. /root/.opam/opam-init/init.fish > /dev/null 2> /dev/null or true
