function fish_title
	if [ $_ = 'fish' ]
        echo (hostname) - (prompt_pwd)
    else
        echo $_
    end
end
