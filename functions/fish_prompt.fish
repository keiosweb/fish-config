function fish_prompt
  #Save the return status of the previous command
  set stat $status

  if not set -q -g __fish_robbyrussell_functions_defined
    set -g __fish_robbyrussell_functions_defined
    function _git_branch_name
      echo (git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
    end
	
    function _is_git_dirty
      echo (git status -s --ignore-submodules=dirty ^/dev/null)
    end
  end

  if not set -q __fish_prompt_hostname
    set -g __fish_prompt_hostname (hostname|cut -d . -f 1)
  end

  set -l cyan (set_color -o cyan)
  set -l yellow (set_color -o yellow)
  set -l green (set_color -o green)
  set -l red (set_color -o red)
  set -l blue (set_color -o blue)
  set -l usercolor (set_color -o blue)
  set -l cwdcolor (set_color $fish_color_cwd)
  set -l normal (set_color normal)

#Set the color for the status depending on the value
  set __fish_color_status (set_color -o blue)
  if test $stat -gt 0
    set __fish_color_status (set_color -o red)
  end

  set -l arrow "➜ "
  set -l cwd $cwdcolor(prompt_pwd)

  if [ (_git_branch_name) ]
    set -l git_branch $red(_git_branch_name)
    set git_info "$blue ($git_branch$blue)"

    if [ (_is_git_dirty) ]
      set -l dirty "$yellow✗"
      set git_info "$git_info$dirty"
    end
  end
  switch $USER

	case root
  echo -n -s $red(hostname)': '$cwd $git_info $blue ' : '
	case '*'

  echo -n -s $usercolor$USER'@'(hostname)': '$cwd $git_info $normal ' ' $__fish_color_status $arrow
end
end

#function fish_right_prompt
#	set -l blue (set_color -o blue)
#	echo -n -s $blue(date "+%H:%M:%S") 
#
#end
