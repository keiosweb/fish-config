function LS
        set dircount (ls|wc -l)
        set dirsize (du -sbh)
	ls -alh $argv
        echo
        echo "Total items: $dircount"
        echo "Total size:  $dirsize"
end
